'use strict';

var ID = 'some fake id';

var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('..');
Database.connect(ID, creds, function(err, db) {
  if(err) {
    console.log(err);
  }

  db.select('Sheet1')
    .where({category: 'computer'})
    .orderBy('date')
    .limit(1)
    .exec(processRows);

  function processRows(err, rows) {
    if(err) console.log(err);

    console.log(rows);
  }
});
