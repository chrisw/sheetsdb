'use strict';

var ID = 'some fake id';

var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('..');
Database.connect(ID, creds, function(err, db) {
  if(err) {
    console.log(err);
  }

  db.update('Sheet1', {name: 'Jane Doe'})
    .where({id: 1})
    .exec(afterUpdate);

  function afterUpdate(err) {
    if(err) console.log(err);

    // continue
  }
});
