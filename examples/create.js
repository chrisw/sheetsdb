'use strict';

var ID = 'some fake id';

var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('..');
Database.connect(ID, creds, function(err, db) {
  if(err) {
    console.log(err);
  }

  db.create('Sheet2', function(err, table) {
    if(err) console.log(err);

    // continue
  });
});
