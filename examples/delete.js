'use strict';

var ID = 'some fake id';

var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('..');
Database.connect(ID, creds, function(err, db) {
  if(err) {
    console.log(err);
  }

  db.delete('Sheet1')
    .where({id: 1})
    .exec(afterDelete);

  function afterDelete(err) {
    if(err) console.log(err);

    // continue
  }
});
