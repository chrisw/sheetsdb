'use strict';

// implementations may vary ***THIS WILL NOT RUN AS IS***
//
// This example uses a populate concept that may be familiar to those
// experienced with NoSQL databases
//
// Please note that this example does not handle circular references

var async = require('async');

// it is assumed here that the Database object has a db attribute
// from which queries and the like may be performed
var Database = require('wherever you have your connected database');

var User = (function() {
  return {
    findById: findById
  };

  function findById(id, cb) {
    Database.db
      .select('users')
      .where({id: id})
      .exec(_populate.bind(null, cb));
  }

  function _populate(cb, err, row) {
    if(err) {
      return cb(err);
    }

    if(!row.length) {
      cb();
    }

    row = row[0];

    row = {
      id: row.get('id'),
      username: row.get('username'),
      password: row.get('password')
    };

    cb(null, row);
  }
})();

var Comment = (function() {
  return {
    findByPost: findByPost
  };

  function findByPost(postId, cb) {
    Database.db
      .select('comments')
      .where({postId: postId})
      .exec(_populate.bind(null, cb));
  }

  function _populate(cb, err, rows) {
    if(err) {
      return cb(err);
    }

    rows = rows.map(function(row) {
      return {
        id: row.get('id'),
        authorId: row.get('authorId'),
        date: row.get('date'),
        body: row.get('body')
      }
    });

    async.each(rows, function(row, done) {
      async.series({
        author: User.findById.bind(null, row.authorId),
      },

      function(err, results) {
        row.author = results.author;

        done(err, row);
      });
    }, cb);
  };
})();

var Post = (function() {
  return {
    findById: findById
  };

  function findById(id, cb) {
    Database.db
      .select('posts')
      .where({id: id})
      .exec(_populate.bind(null, cb));
  }

  function _populate(cb, err, row) {
    if(err) {
      return cb(err);
    }

    if(!row.length) {
      return cb();
    }

    row = row[0];

    row = {
      id: row.get('id'),
      authorId: row.get('authorId'),
      date: row.get('date'),
      body: row.get('body')
    };

    async.series({
      author: User.findById.bind(null, row.authorId),
      comments: Comment.findByPost.bind(null, row.id)
    },

    function(err, results) {
      row.author = results.author;
      row.comments = results.comments;

      cb(err, row);
    });
  }
})();

Post.findById(1, function(err,  post) {
  console.log(JSON.stringify(post));

  /*
   * {
   *   id: 1,
   *   authorId: 1,
   *   date: 0,
   *   body: 'This is a post',
   *
   *   author: {
   *     id: 1,
   *     username: 'John Doe',
   *     password: 'password'
   *   },
   *
   *   comments: [
   *     {
   *       id: 1,
   *       authorId: 2,
   *       date: 1,
   *       body: 'This is a comment',
   *
   *       author: {
   *         id: 2,
   *         username: 'Jane Doe',
   *         password: 'password'
   *       }
   *     }
   *   ]
   * }
   *
   * Please not that the raw number of milliseconds is used for date
  */
});
