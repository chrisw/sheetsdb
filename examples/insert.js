'use strict';

var ID = 'some fake id';

var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('..');
Database.connect(ID, creds, function(err, db) {
  if(err) {
    console.log(err);
  }

  db.insert('Sheet1', {id: 1, name: 'John Doe'}, function(err) {
    if(err) console.log(err);

    // continue
  });
});
