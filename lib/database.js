/**
 * @module sheetsdb/database
 */

'use strict';

var Table = require('./table');
var SpreadSheet = require('./google/spreadsheet');

/**
 * Connect to a spreadsheet
 * .connect
 * @static
 * @param {object} creds
 * @param {function} cb
 */
Database.connect = function(key, creds, cb) {
  var spreadsheet = new SpreadSheet(key);

  spreadsheet.connect(creds, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, new Database(spreadsheet));
  });
};

/**
 * Creates an instance of Database
 * @constructor
 * @param {object} spreadsheet
 */
function Database(spreadsheet) {
  var self = this;

  self.spreadsheet = spreadsheet;
  self.tables = {};

  self._setTables();
}

/**
 * Set Tables already created
 * #_setTables
 */
Database.prototype._setTables = function() {
  var self = this;

  self.spreadsheet.worksheets.forEach(function(worksheet) {
    self.tables[worksheet.title] = new Table(worksheet);
  });
};


/**
 * Create a new Table
 * #create
 * @param {string} tableName
 * @param {array} tableSchema
 * @param {function} cb
 */
Database.prototype.create = function(tableName, cb) {
  var self = this;

  if(self.tables[tableName]) {
    return cb(new Error('Table: ' + tableName + ' already exists'));
  }

  self.spreadsheet.createWorkSheet(tableName, 100, function(err, worksheet) {
    if(err) {
      return cb(err);
    }

    self.tables[tableName] = new Table(worksheet);
    cb(null, self.tables[tableName]);
  });
};

/**
 * Insert Table
 * #insert
 * @param {string} tableName
 * @param {object} rowObject
 * @param {function} cb
 */
Database.prototype.insert = function(tableName, rowObject, cb) {
  var self = this;

  if(!self.tables[tableName]) {
    throw new Error('Table: ' + tableName + ' not found');
  }

  self.tables[tableName].insert(rowObject, cb);
};

/**
 * Select Table
 * #select
 * @param {string} tableName
 * @returns {object}
 */
Database.prototype.select = function(tableName) {
  var self = this;

  if(!self.tables[tableName]) {
    throw new Error('Table: ' + tableName + ' not found');
  }

  return self.tables[tableName].select();
};

/**
 * Update Table
 * #update
 * @param {string} tableName
 * @param {object} update
 * @returns {object}
 */
Database.prototype.update = function(tableName, update) {
  var self = this;

  if(!self.tables[tableName]) {
    throw new Error('Table: ' + tableName + ' not found');
  }

  return self.tables[tableName].update(update);
};

/**
 * Delete Table
 * #delete
 * @param {string} tableName
 * @returns {object}
 */
Database.prototype.delete = function(tableName) {
  var self = this;

  if(!self.tables[tableName]) {
    throw new Error('Table: ' + tableName + ' not found');
  }

  return self.tables[tableName].delete();
};

/**
 * Drop Table
 * #drop
 * @param {string} tableName
 * @param {function} cb
 */
Database.prototype.drop = function(tableName, cb) {
  var self = this;

  if(!self.tables[tableName]) {
    return cb(new Error('Table: ' + tableName + ' not found'));
  }

  self.tables[tableName].drop(function(err) {
    if(err) {
      return cb(err);
    }

    delete self.tables[tableName];
    cb();
  });
};

/**
 * Get Table by name
 * #table
 * @param {string} tableName
 */
Database.prototype.table = function(tableName) {
  var self = this;

  if(!self.tables[tableName]) {
    throw new Error('Table: ' + tableName + ' not found');
  }

  return self.tables[tableName];
}

module.exports = Database;
