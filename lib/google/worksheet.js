/**
 * @module sheetsdb/worksheet
 */

'use strict';

var Row = require('./row');
var Util = require('./util');

/**
 * Creates an instance of WorkSheet
 * @constructor
 * @param {object} spreadsheet
 * @param {object} data
 */
function WorkSheet(spreadsheet, data) {
  var self = this;

  self.spreadsheet = spreadsheet;
  self.id = Util.forceSingle(data.id).split('/').pop();
  self.title = Util.forceSingle(data.title)['_'];

  self._extractLinks(data);
}

/**
 * Extract all links from data
 * #_extractLinks
 * @param {object} data
 */
WorkSheet.prototype._extractLinks = function(data) {
  var self = this;
  self.links = {};

  Util.forceArray(data.link).forEach(function(link) {
    self.links[link.$.rel] = link.$.href;
  });
};

/**
 * Query WorkSheet rows
 * #getRows
 * @param {object} query
 * @param {function} cb
 */
function getRows(query, cb) {
  var self = this;

  var options = {
    url: ['list', self.spreadsheet.key, self.id],
    method: 'GET',
    query: query
  };

  Util.feedRequest(self.spreadsheet.token, options, function(err, data) {
    if(err) {
      return cb(err);
    }

    self._dataToRows(data, cb);
  });
};

WorkSheet.prototype.getRows = Util.renewAuth(getRows);

/**
 * Converts data to rows
 * #_dataToRows
 * @param {object} data
 * @param {function} cb
 */
WorkSheet.prototype._dataToRows = function(data, cb) {
  var self = this;

  var rows = [];
  Util.forceArray(data.entry).forEach(function(entry) {
    if(entry) {
      rows.push(new Row(self.spreadsheet, entry));
    }
  });

  cb(null, rows);
};

/**
 * Add a row to the WorkSheet
 * #addRow
 * @param {obejct} row
 * @param {function} cb
 */
function addRow(row, cb) {
  var self = this;

  var body = '<entry xmlns="http://www.w3.org/2005/Atom" ' +
             'xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended">';

  Object.keys(row).forEach(function(key) {
    var safeKey = Util.xmlSafe(key);
    body += '<gsx:' + safeKey + '>' + row[key] + '</gsx:' + safeKey + '>'
  });

  body += '</entry>';

  var options = {
    url: ['list', self.spreadsheet.key, self.id],
    method: 'POST',
    body: body
  };

  Util.feedRequest(self.spreadsheet.token, options, cb);
};

WorkSheet.prototype.addRow = Util.renewAuth(addRow);

/**
 * Delete the Worksheet
 * #del
 * @param {function} cb
 */
function del(cb) {
  var self = this;

  var options = {
    url: self.links.edit,
    method: 'DELETE'
  };

  Util.feedRequest(self.spreadsheet.token, options, cb);
};

WorkSheet.prototype.del = Util.renewAuth(del);

module.exports = WorkSheet;
