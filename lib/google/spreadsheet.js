/**
 * @module sheetsdb/spreadsheet
 */

'use strict';

var GoogleAuth = require('google-auth-library');
var googleAuth = new GoogleAuth();

var WorkSheet = require('./worksheet');
var Util = require('./util');

var AUTH_SCOPE = ['https://spreadsheets.google.com/feeds'];

/**
 * Creates an instance of SpreadSheet
 * @constructor
 */
function SpreadSheet(key) {
  var self = this;

  self.key = key;
  self.worksheets = [];
}

/**
 * Connects SpreadSheet to Google Sheet
 * #connect
 * @param {object} creds
 * @param {function} cb
 */
SpreadSheet.prototype.connect = function(creds, cb) {
  var self = this;

  self.client = new googleAuth.JWT(creds.client_email, null, creds.private_key,
                                   AUTH_SCOPE, null);
  self._authenticate(function(err) {
    if(err) {
      return cb(err);
    }

    self._populateWorkSheets(cb);
  });
};

/**
 * Authenticate client
 * #_authenticate
 * @param {function} cb
 */
SpreadSheet.prototype._authenticate = function(cb) {
  var self = this;

  self.client.authorize(function(err, token) {
    if(err) {
      return cb(err);
    }

    self.token = token;
    cb();
  });
};

/**
 * Populate worksheets instance variable
 * #_populateWorkSheets
 * @param {function} cb
 */
SpreadSheet.prototype._populateWorkSheets = function(cb) {
  var self = this;

  var options = {
    url: ['worksheets', self.key],
    method: 'GET'
  };

  Util.feedRequest(self.token, options, function(err, data) {
    if(err) {
      return cb(err);
    }

    if(!data) {
      return cb(new Error('No worksheets response'));
    }

    self.worksheets = self._dataToWorkSheets(data);

    cb();
  });
};

/**
 * Convert data to WorkSheets
 * #_dataToWorkSheets
 * @param {object} data
 * @returns {array}
 */
SpreadSheet.prototype._dataToWorkSheets = function(data) {
  var self = this;

  var worksheets = [];
  Util.forceArray(data.entry).forEach(function(entry) {
    if(entry) {
      worksheets.push(new WorkSheet(self, entry));
    }
  });

  return worksheets;
};

/**
 * Creates a new Google Worksheet
 * #createWorkSheet
 * @param {string} worksheetTitle
 * @param {function} cb
 */
SpreadSheet.prototype.createWorkSheet = function(worksheetTitle, numCols, cb) {
  // TODO figure out how to set header row of worksheet
  // this needs to be done using a cell feed
  var self = this;

  var body = '<entry xmlns="http://www.w3.org/2005/Atom" ' +
             'xmlns:gs="http://schemas.google.com/spreadsheets/2006">' +
             '<title>' + worksheetTitle + '</title>' +
             '<gs:rowCount>10</gs:rowCount>' +
             '<gs:colCount>' + numCols + '</gs:colCount>' +
             '</entry>';

  var options = {
    url: ['worksheets', self.key],
    method: 'POST',
    body: body
  };

  Util.feedRequest(self.token, options, function(err) {
    if(err) {
      return cb(err);
    }

    self._populateWorkSheets(function(err) {
      if(err) {
        return cb(err);
      }

      cb(null, self.worksheets[self.worksheets.length - 1]);
    });
  });
};

module.exports = SpreadSheet;
