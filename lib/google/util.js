/**
 * @module sheetsdb/util
 */

'use strict';

var querystring = require('querystring');
var request = require('request');

var xml2js = require('xml2js');
var xmlParser = new xml2js.Parser();
var xmlBuilder = new xml2js.Builder();

// static variables
var FEED_URL = 'https://spreadsheets.google.com/feeds/';
var AUTH_SCOPE = ['http://spreadsheets.google.com/feeds/'];

var VISIBILITY = 'private';
var PROJECTION = 'full';

/**
 * Decorates a given function to renew auth token prior to execution
 * @param {function} func
 * @returns {function}
 */
function renewAuth(func) {
  return function() {
    var self = this;
    var args = arguments;
    var cb = args[args.length - 1];

    // this could be more robust
    if(self.spreadsheet.token.expiry_date > new Date().getTime()) {
      return func.apply(self, args);
    }

    self.spreadsheet._authenticate(function(err) {
      if(err) {
        if(typeof cb === 'function') {
          return cb(err);
        } else {
          throw err;
        }
      }

      func.apply(self, args);
    });
  }
}

/**
 * Requests feed from spreadsheet/worksheet
 * @param {object} token
 * @param {object} options
 * @param {function} cb
 */
function feedRequest(token, options, cb) {
  if(!options.url) {
    return cb(new Error('Request Feed must be given url params'));
  }
  if(!options.method) {
    return cb(new Error('Request Feed must be given http method'));
  }

  var url;
  if(Array.isArray(options.url)) {
    url = FEED_URL;

    options.url.push(VISIBILITY, PROJECTION);
    url += options.url.join('/');
  } else {
    url = options.url;
  }

  if(options.method === 'GET' && options.query) {
    url += '?' + querystring.stringify(options.query);
  }

  var headers = {
    Authorization: 'Bearer ' + token.access_token
  };

  if(options.method === 'POST' || options.method === 'PUT') {
    headers['content-type'] = 'application/atom+xml';
  }

  var requestOptions = {
    url: url,
    headers: headers,
    method: options.method,
    body: options.body || null,
  };

  request(requestOptions, function(err, res, body) {
    if(err) {
      return cb(err);
    }
    if(res.statusCode !== 200 && res.statusCode !== 201) {
      return cb(new Error('Request failed with status ' + res.statusCode));
    }

    if(body) {
      xmlParser.parseString(body, function(err, data) {
        if(err) {
          return cb(err);
        }

        cb(null, data.feed, body);
      });
    } else {
      cb(null);
    }
  });
}

/**
 * Coerses a value to an xml safe representation
 * @param {any} value
 * @returns {string}
 */
function xmlSafe(value) {
  if(!value) {
    return '';
  }

  return String(value).replace(/[\s]/g, '').toLowerCase();
}

/**
 * Coerses a value to an array
 * @param {any} value
 * @returns {array}
 */
function forceArray(value) {
  return (Array.isArray(value)) ? value : [value];
}

/**
 * Coerses an array to a single value
 * @param {array} value
 * @returns {any}
 */
function forceSingle(value) {
  return (Array.isArray(value)) ? value[0] : value;
}

module.exports = {
  renewAuth: renewAuth,
  feedRequest: feedRequest,
  js2xml: xmlBuilder.buildObject.bind(xmlBuilder),
  xmlSafe: xmlSafe,
  forceArray: forceArray,
  forceSingle: forceSingle
};
