/**
 * @module sheetsdb/row
 */

'use strict';

var Util = require('./util');

/**
 * Creates an instance of Row
 * @constructor
 * @param {object} spreadsheet
 * @param {object} data
 */
function Row(spreadsheet, data) {
  var self = this;

  self.spreadsheet = spreadsheet;
  self.data = data;
  self._extractAttributes(data);
  self._extractLinks(data);
}

/**
 * Extract all attributes from data
 * #_extractAttributes
 * @param {object} data
 */
Row.prototype._extractAttributes = function(data) {
  var self = this;
  self.attr = {};

  Object.keys(data).forEach(function(key) {
    if(!key.indexOf('gsx:')) {
      self.attr[key.substring(4)] = Util.forceSingle(data[key]);
    }
  });
};

/**
 * Extract all links from data
 * #_extractLinks
 * @param {object} data
 */
Row.prototype._extractLinks = function(data) {
  var self = this;
  self.links = {};

  Util.forceArray(data.link).forEach(function(link) {
    self.links[link.$.rel] = link.$.href;
  });
};

/**
 * Get attribute from Row
 * #get
 * @param {string} attr
 * @returns {string}
 */
Row.prototype.get = function(attr) {
  var self = this;

  return self.attr[Util.xmlSafe(attr)];
}

/**
 * Set attribute on Row
 * #get
 * @param {string} attr
 * @param {any} value
 */
Row.prototype.set = function(attr, value) {
  var self = this;

  self.attr[Util.xmlSafe(attr)] = value;
}

/**
 * Save changes made to Row
 * #save
 * @param {function} cb
 */
function save(cb) {
  var self = this;

  for (var key in self.attr) {
    self.data['gsx:' + key] = Util.forceArray(self.attr[key]);
  }

  self.data['$'] = {
    'xmlns': 'http://www.w3.org/2005/Atom',
    'xmlns:gsx': 'http://schemas.google.com/spreadsheets/2006/extended'
  };

  var objXml = {entry: self.data};

  var options = {
    url: self.links.edit,
    method: 'PUT',
    body: Util.js2xml(objXml)
  };

  Util.feedRequest(self.spreadsheet.token, options, cb);
};

Row.prototype.save = Util.renewAuth(save);

/**
 * Delete the Row
 * #del
 * @param {function} cb
 */
function del(cb) {
  var self = this;

  var options = {
    url: self.links.edit,
    method: 'DELETE'
  };

  Util.feedRequest(self.spreadsheet.token, options, cb);
};

Row.prototype.del = Util.renewAuth(del);

module.exports = Row;
