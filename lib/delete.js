/**
 * @module sheetsdb/delete
 */

'use strict';

var async = require('async');
var Query = require('./query');
var Util = require('./google/util');

/**
 * Creates an instance of Delete
 * @constructor
 * @param {object} worksheet
 */
function Delete(worksheet, query) {
  var self = this;

  self.worksheet = worksheet;

  if(query) {
    self.query = new Query(worksheet, query.query);
  } else {
    self.query = new Query(worksheet, query);
  }
}

/**
 * Adds WHERE clause
 * #where
 * @param {object} opts
 * @returns {object}
 */
Delete.prototype.where = function(opts) {
  var self = this;

  return new Delete(self.worksheet, self.query.where(opts));
};

/**
 * Adds ORDER BY clause
 * #orderBy
 * @param {string} clause
 * @returns {object}
 */
Delete.prototype.orderBy = function(clause) {
  var self = this;

  return new Delete(self.worksheet, self.query.orderBy(clause));
};

/**
 * Adds DESC clause
 * #reverse
 * @returns {object}
 */
Delete.prototype.reverse = function() {
  var self = this;

  return new Delete(self.worksheet, self.query.reverse());
};

/**
 * Adds LIMIT clause
 * #limit
 * @param {number} clause
 * @returns {object}
 */
Delete.prototype.limit = function(clause) {
  var self = this;

  return new Delete(self.worksheet, self.query.limit(clause));
};

/**
 * Adds OFFSET clause
 * Note: this offsets the worksheet, not the results
 * #offset
 * @param {number} clause
 * @returns {object}
 */
Delete.prototype.offset = function(clause) {
  var self = this;

  return new Delete(self.worksheet, self.query.offset(clause));
};

/**
 * Executes Delete
 * #exec
 * @param {function} cb
 */
Delete.prototype.exec = function(cb) {
  var self = this;

  self.query.exec(function(err, rows) {
    if(err) {
      return cb(err);
    }

    async.each(rows, function(row, cb) {
      row.del(cb);
    }, cb);
  });
};

module.exports = Delete;
