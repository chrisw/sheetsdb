/**
 * @module sheetsdb/query
 */

'use strict';

var Util = require('./google/util');

/**
 * Creates an instance of Query
 * @constructor
 * @param {object} worksheet
 */
function Query(worksheet, query) {
  var self = this;

  self.worksheet = worksheet;

  if(query) {
    self.query = JSON.parse(JSON.stringify(query));
  } else {
    self.query = {};
  }
}

/**
 * Adds WHERE clause
 * #where
 * @param {object} opts
 * @returns {object}
 */
Query.prototype.where = function(opts) {
  var self = this;
  var clause = self._parseWhere(opts);

  var newQuery = new Query(self.worksheet, self.query);
  newQuery.query['sq'] = clause;
  return newQuery;
};

/**
 * Parse Options into a WHERE clause
 * #_parseWhere
 * @param {object} opts
 * @returns {string}
 */
Query.prototype._parseWhere = function(opts) {
  var clause = [];

  Object.keys(opts).forEach(function(key) {
    var value = String(opts[key]);
    if(!value.match(/^((\d+)|(\d*[.]\d+))$/)) {
      value = '"' + value + '"';
    }

    var cond = Util.xmlSafe(key) + " = " + value;
    clause.push(cond);
  });

  return clause.join(' and ');
};

/**
 * Adds ORDER BY clause
 * #orderBy
 * @param {string} clause
 * @returns {object}
 */
Query.prototype.orderBy = function(clause) {
  var self = this;

  var newQuery = new Query(self.worksheet, self.query);
  newQuery.query['orderby'] = clause;
  return newQuery;
};

/**
 * Adds DESC clause
 * #reverse
 * @returns {object}
 */
Query.prototype.reverse = function() {
  var self = this;

  var newQuery = new Query(self.worksheet, self.query);
  newQuery.query['reverse'] = true;
  return newQuery;
};

/**
 * Adds LIMIT clause
 * #limit
 * @param {number} clause
 * @returns {object}
 */
Query.prototype.limit = function(clause) {
  var self = this;

  var newQuery = new Query(self.worksheet, self.query);
  newQuery.query['max-results'] = clause;
  return newQuery;
};

/**
 * Adds OFFSET clause
 * Note: this offsets the worksheet, not the results
 * #offset
 * @param {number} clause
 * @returns {object}
 */
Query.prototype.offset = function(clause) {
  var self = this;

  var newQuery = new Query(self.worksheet, self.query);
  newQuery.query['start-index'] = clause;
  return newQuery;
};

/**
 * Executes query
 * #exec
 * @param {function} cb
 */
Query.prototype.exec = function(cb) {
  var self = this;

  self.worksheet.getRows(self.query, cb);
};

module.exports = Query;
