/**
 * @module sheetsdb/table
 */

'use strict';

var Query = require('./query');
var Update = require('./update');
var Delete = require('./delete');

/**
 * Creates an instance of Table
 * @constructor
 * @param {object} worksheet
 */
function Table(worksheet) {
  var self = this;
  self.worksheet = worksheet;
}

/**
 * Insert row into Table
 * #insert
 * @param {object} rowObject
 * @param {function} cb
 */
Table.prototype.insert = function(rowObject, cb) {
  var self = this;

  self.worksheet.addRow(rowObject, cb);
};

/**
 * Query rows from Table
 * #select
 * @returns {object}
 */
Table.prototype.select = function() {
  var self = this;

  return new Query(self.worksheet);
};

/**
 * Update rows from Table
 * #update
 * @param {object} update
 * @returns {object}
 */
Table.prototype.update = function(update) {
  var self = this;

  return new Update(self.worksheet, update);
};

/**
 * Delete rows from Table
 * #delete
 * @returns {object}
 */
Table.prototype.delete = function() {
  var self = this;

  return new Delete(self.worksheet);
};

/**
 * Drop the Table
 * #drop
 * @param {function} cb
 */
Table.prototype.drop = function(cb) {
  var self = this;

  self.worksheet.del(cb);
};

module.exports = Table;
