/**
 * @module sheetsdb/update
 */

'use strict';

var async = require('async');
var Query = require('./query');

/**
 * Creates an instance of Update
 * @constructor
 * @param {object} worksheet
 * @param {object} update
 */
function Update(worksheet, update, query) {
  var self = this;

  self.update = update || {};
  self.worksheet = worksheet;

  if(query) {
    self.query = new Query(worksheet, query.query);
  } else {
    self.query = new Query(worksheet, query);
  }
}

/**
 * Adds WHERE clause
 * #where
 * @param {object} opts
 * @returns {object}
 */
Update.prototype.where = function(opts) {
  var self = this;

  return new Update(self.worksheet, self.update,
                    self.query.where(opts));
};

/**
 * Adds ORDER BY clause
 * #orderBy
 * @param {string} clause
 * @returns {object}
 */
Update.prototype.orderBy = function(clause) {
  var self = this;

  return new Update(self.worksheet, self.update,
                    self.query.orderBy(clause));
};


/**
 * Adds DESC clause
 * #reverse
 * @returns {object}
 */
Update.prototype.reverse = function() {
  var self = this;

  return new Update(self.worksheet, self.update,
                    self.query.reverse());
};

/**
 * Adds LIMIT clause
 * #limit
 * @param {number} clause
 * @returns {object}
 */
Update.prototype.limit = function(clause) {
  var self = this;

  return new Update(self.worksheet, self.update,
                    self.query.limit(clause));
};

/**
 * Adds OFFSET clause
 * Note: this offsets the worksheet, not the results
 * #offset
 * @param {number} clause
 * @returns {object}
 */
Update.prototype.offset = function(clause) {
  var self = this;

  return new Update(self.worksheet, self.update,
                    self.query.offset(clause));
};

/**
 * Executes Update
 * #exec
 * @param {function} cb
 */
Update.prototype.exec = function(cb) {
  var self = this;

  self.query.exec(function(err, rows) {
    if(err) {
      return cb(err);
    }

    async.each(rows, function(row, cb) {
      Object.keys(self.update).forEach(function(key) {
        row.set(key, self.update[key]);
      });

      row.save(cb);
    }, cb);
  });
};

module.exports = Update;
