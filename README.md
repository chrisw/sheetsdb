# Topics

- [Introduction](#introduction)
- [Setup](#setup)
- [Installation](#installation)
- [Basic Examples](#basic-examples)
    - [Create](#create)
    - [Insert](#insert)
    - [Select](#select)
    - [Update](#update)
    - [Delete](#delete)
- [Joins](#joins)
- [Accessible Classes](#accessible-classes)
    - [Database](#database)
    - [Table](#table)
    - [Row](#row)
    - [Query/Update/Delete](#queryupdatedelete)
- [License](#license)

# Introduction
This tool is designed to turn Google Sheets into a feasible database, providing
a wrapper to the Sheets API that is more condusive to database interactions.

# Setup
To use this tool, you must first set up a service account on the Google
Developers Console. 

1. Create a project

2. Select Add Credentials, and select Service Account. 
(Make sure to create a JSON key type)

3. Once the account has been created, generate a new JSON key. This will be used 
as credentials to connect to the Google Sheet.

4. Create a new Google Sheet. Note the long string of characters in the url.
This is the id by which the sheet can be connected.

5. Share the new Sheet with the service account, allowing edit access to the
service credentials

# Installation

    npm install sheetsdb


# Basic Examples

## Create
```javascript
// that long string of characters in the sheet\'s url
var ID = 'sheet key';

// can be retrieved from the downloaded json file for your service account
var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('sheetsdb');
Database.connect(ID, creds, function(err, db) {
  if(err) console.log(err);
  
  db.create('Sheet2', function(err, table) {
    if(err) console.log(err);
    
    // new table has been created
  });
});
```

## Insert
```javascript
// that long string of characters in the sheet\'s url
var ID = 'sheet key';

// can be retrieved from the downloaded json file for your service account
var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('sheetsdb');
Database.connect(ID, creds, function(err, db) {
  if(err) console.log(err);
  
  db.insert('Sheet1', {id:1, name:'John Doe'}, function(err) {
    if(err) console.log(err);
    
    // row has been inserted into table
  });
});
```

## Select
```javascript
// that long string of characters in the sheet\'s url
var ID = 'sheet key';

// can be retrieved from the downloaded json file for your service account
var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('sheetsdb');
Database.connect(ID, creds, function(err, db) {
  if(err) console.log(err);
  
  db.select('Sheet1').where({id: 1}).exec(function(err, rows) {
    if(err) console.log(err);
    
    // continue working with the newly retrieved rows
  });
});
```

## Update
```javascript
// that long string of characters in the sheet\'s url
var ID = 'sheet key';

// can be retrieved from the downloaded json file for your service account
var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('sheetsdb');
Database.connect(ID, creds, function(err, db) {
  if(err) console.log(err);
  
  db.update('Sheet1', {id: 2}).where({id: 1}).exec(function(err) {
    if(err) console.log(err);
    
    // rows whose attribute id was 1 now have an id of 2
  });
});
```

## Delete
```javascript
// that long string of characters in the sheet\'s url
var ID = 'sheet key';

// can be retrieved from the downloaded json file for your service account
var creds = {
  client_email: 'email',
  private_key: 'key'
};

var Database = require('sheetsdb');
Database.connect(ID, creds, function(err, db) {
  if(err) console.log(err);
  
  db.delete('Sheet1').where({id: 1}).exec(function(err) {
    if(err) console.log(err);
    
    // rows in which attribute id == 1 have been deleted
  });
});
```

# Joins

It is important to note that joins are not currently implemented and should
not be expected soon given the nature of the google sheets api. There are
feasible workarounds, however, one of which can be found
[here](examples/populate.js).

# Accessible Clases

## [Database](lib/database.js)

### Static Methods
__connect(id:String, creds:Object, callback:Function)__

connects to the given spreadsheet using the provided credentials

Passes Back (err, db) where db is an instance of Database


### Instance Methods
__table(tableName:String):Table__

return the table of the given name


__create(tableName:String, callback:Function)__ 

creates and a new Table with the given name

Passes back the new Table


__insert(tableName:String row:Object, callback:Function)__

inserts a new row with specified attributes into the table given


__select(tableName:String):Select__

returns a Select Object for the given table


__update(tabeName:String, update:Object):Update__

returns an Update Object for the given table


__delete(tableName:String):Delete__

returns a Delete Object for the given table


## [Table](lib/table.js)

### Instance Methods

__insert(tableName:String row:Object, callback:Function)__

inserts a new row with specified attributes into the table


__select(tableName:String):Select__

returns a Select Object for the table


__update(tabeName:String, update:Object):Update__

returns an Update Object for the table


__delete(tableName:String):Delete__

returns a Delete Object for the table


__drop(callback:Function)__

drops the table


## [Row](lib/google/row.js)

### Instance Methods

__get(attribute:String)__

retrieves the value for the sttribute specified

__set(attributeKey:String, attributeValue:Any)__

sets the value of the attribute specified

(All values are saved as Strings unless modified)

__save(callback:Function)__

saves the current state of the Row Object back to to Google Sheet

__del(callback:Function)__

deletes the Row from the Google Sheet


## [Query](lib/query.js)/[Update](lib/update.js)/[Delete](lib/delete.js)

### Instance Methods

__where(clause:Object):Query/Update/Delete__

Adds a 'where' clause to the statement


__orderBy(clause:String):Query/Update/Delete__

Orders the rows retrieved by the specified column


__limit(clause:String):Query/Update/Delete__

Limits the number of rows affected by the specified amount


__offset(clause:String):Query/Update/Delete__

Offsets the row retrieval by the indicated value


__exec(callback:Function)__

Executes the query/update/delete

Query passes back the filtered rows

# License
The MIT License (MIT)

Copyright (c) 2015 Chris Whelan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.