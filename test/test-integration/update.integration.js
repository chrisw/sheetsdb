
'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Update = require('../../lib/update');

describe('Update', function() {
  describe('chaining', function() {
    it('should chain clauses', function() {
      var update = new Update({a: 1})
                  .where({where: 'here'})
                  .orderBy('order')
                  .reverse()
                  .limit('limit')
                  .offset('off');

      expect(update.query.query).to.have.property('sq');
      expect(update.query.query).to.have.property('orderby');
      expect(update.query.query).to.have.property('reverse');
      expect(update.query.query).to.have.property('max-results');
      expect(update.query.query).to.have.property('start-index');
    });
  });
});
