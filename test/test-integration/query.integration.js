
'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Query = require('../../lib/query');

describe('Query', function() {
  describe('chaining', function() {
    it('should chain clauses', function() {
      var query = new Query()
                  .where({where: 'here'})
                  .orderBy('order')
                  .reverse()
                  .limit('limit')
                  .offset('off');

      expect(query.query).to.have.property('sq');
      expect(query.query).to.have.property('orderby');
      expect(query.query).to.have.property('reverse');
      expect(query.query).to.have.property('max-results');
      expect(query.query).to.have.property('start-index');
    });
  });
});
