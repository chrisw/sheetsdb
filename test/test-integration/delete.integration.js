
'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Delete = require('../../lib/delete');

describe('Delete', function() {
  describe('chaining', function() {
    it('should chain clauses', function() {
      var del = new Delete()
                  .where({where: 'here'})
                  .orderBy('order')
                  .reverse()
                  .limit('limit')
                  .offset('off');

      expect(del.query.query).to.have.property('sq');
      expect(del.query.query).to.have.property('orderby');
      expect(del.query.query).to.have.property('reverse');
      expect(del.query.query).to.have.property('max-results');
      expect(del.query.query).to.have.property('start-index');
    });
  });
});
