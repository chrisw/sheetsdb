'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Update = require('../../lib/update');
var Query = require('../../lib/query');

describe('Update', function() {
  describe('@constructor', function() {
    it('should create an instance of Update', function() {
      expect(new Update()).to.be.instanceOf(Update);
    });

    it('should provide empty update if one is not given', function() {
      var update = new Update();
      expect(update.update).to.be.empty;
    });

    it('should create a copy of query if provided', function() {
      var update = new Update(null, null, {query: {where: 'where'}});
      expect(update.query).to.be.instanceOf(Query);
    });
  });

  describe('#where', function() {
    it('should return a new Update', function() {
      var update = new Update();
      var newUpdate = update.where({where: 'here'});
      expect(newUpdate).to.be.instanceOf(Update);
      expect(update).to.not.equal(newUpdate);
    });

    it('should update `query` of Query', function() {
      var update = new Update();
      var newUpdate = update.where({where: 'here'});
      expect(newUpdate.query.query.sq).to.equal('where = "here"');
    });
  });

  describe('#orderBy', function() {
    it('should return a new Update', function() {
      var update = new Update();
      var newUpdate = update.orderBy();
      expect(newUpdate).to.be.instanceOf(Update);
      expect(update).to.not.equal(newUpdate);
    });

    it('should update `query` of Query', function() {
      var update = new Update();
      var newUpdate = update.orderBy('orderBy');
      expect(newUpdate.query.query.orderby).to.equal('orderBy');
    });
  });

  describe('#reverse', function() {
    it('should return a new Update', function() {
      var update = new Update();
      var newUpdate = update.reverse();
      expect(newUpdate).to.be.instanceOf(Update);
      expect(update).to.not.equal(newUpdate);
    });

    it('should update `query` of Query', function() {
      var update = new Update();
      var newUpdate = update.reverse();
      expect(newUpdate.query.query.reverse).to.be.true;
    });
  });

  describe('#limit', function() {
    it('should return a new Update', function() {
      var update = new Update();
      var newUpdate = update.limit();
      expect(newUpdate).to.be.instanceOf(Update);
      expect(update).to.not.equal(newUpdate);
    });

    it('should update `query` of Query', function() {
      var update = new Update();
      var newUpdate = update.limit('limit');
      expect(newUpdate.query.query['max-results']).to.equal('limit');
    });
  });

  describe('#offset', function() {
    it('should return a new Update', function() {
      var update = new Update();
      var newUpdate = update.offset();
      expect(newUpdate).to.be.instanceOf(Update);
      expect(update).to.not.equal(newUpdate);
    });

    it('should update `query` of Query', function() {
      var update = new Update();
      var newUpdate = update.offset('offset');
      expect(newUpdate.query.query['start-index']).to.equal('offset');
    });
  });

  describe('#exec', function() {
    var rows;

    function save(cb) {
      cb();
    }

    var worksheet = {
      getRows: function(query, cb) {
        cb(null, rows);
      }
    };
    sinon.spy(worksheet, 'getRows');

    beforeEach(function() {
      rows = [
        {save: save, get: function() {},
          set: function(k, v) {this.attr[k] = v;}, attr: {}},
        {save: save, get: function() {},
          set: function(k, v) {this.attr[k] = v;}, attr: {}}
      ];

      worksheet.getRows.reset();
    });

    it('should query `worksheet` for rows', function(done) {
      var update = new Update(worksheet);
      update.exec(function() {
        expect(worksheet.getRows).to.have.been.called;
        done();
      });
    });

    it('should update each row received by the query', function(done) {
      var update = new Update(worksheet, {name: 'name'});
      update.exec(function() {
        expect(rows[0].attr).to.have.property('name');
        expect(rows[0].attr.name).to.equal('name');
        expect(rows[1].attr).to.have.property('name');
        expect(rows[1].attr.name).to.equal('name');
        done();
      });
    });

    it('should pass back an error if query fails', function(done) {
      var getRows = worksheet.getRows;
      worksheet.getRows = function(query, cb) {
        cb(new Error('failure'));
      };

      var update = new Update(worksheet);
      update.exec(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        worksheet.getRows = getRows;
        done();
      });
    });
  });
});
