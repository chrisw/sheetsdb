'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Query = require('../../lib/query');

describe('Query', function() {
  describe('@constructor', function() {
    it('should create an instance of Query', function() {
      expect(new Query()).to.be.instanceOf(Query);
    });

    it('should create an copy of query if provided', function() {
      var query = new Query(null, {where: 'where'});
      expect(query.query.where).to.equal('where');
    });
  });

  describe('#where', function() {
    it('should return a new Query', function() {
      var query = new Query();
      var newQuery = query.where({where: 'here'});
      expect(newQuery).to.be.instanceOf(Query);
      expect(query).to.not.equal(newQuery);
    });

    it('should update `query` of new Query', function() {
      var query = new Query();
      var newQuery = query.where({where: 'here'});
      expect(newQuery.query).to.have.property('sq');
      expect(newQuery.query.sq).to.equal('where = "here"');
    });
  });

  describe('#_parseWhere', function() {
    it('should convert opts to a simple WHERE clause', function() {
      var opts = {
        user: 'user'
      };

      expect(new Query()._parseWhere(opts)).to.equal('user = "user"');
    });

    it('should handle more than one opt', function() {
      var opts = {
        user: 'u',
        pass: 'p'
      };

      expect(new Query()._parseWhere(opts)).to.equal('user = "u" and pass = "p"');
    });
  });

  describe('#orderBy', function() {
    it('should return a new Query', function() {
      var query = new Query();
      var newQuery = query.orderBy();
      expect(newQuery).to.be.instanceOf(Query);
      expect(query).to.not.equal(newQuery);
    });

    it('should update `query` of new Query', function() {
      var query = new Query();
      var newQuery = query.orderBy('orderBy');
      expect(newQuery.query).to.have.property('orderby');
      expect(newQuery.query.orderby).to.equal('orderBy');
    });
  });

  describe('#reverse', function() {
    it('should return a new Query', function() {
      var query = new Query();
      var newQuery = query.reverse();
      expect(newQuery).to.be.instanceOf(Query);
      expect(query).to.not.equal(newQuery);
    });

    it('should update `query` of new Query', function() {
      var query = new Query();
      var newQuery = query.reverse();
      expect(newQuery.query).to.have.property('reverse');
      expect(newQuery.query['reverse']).to.be.true;
    });
  });

  describe('#limit', function() {
    it('should return a new Query', function() {
      var query = new Query();
      var newQuery = query.limit();
      expect(newQuery).to.be.instanceOf(Query);
      expect(query).to.not.equal(newQuery);
    });

    it('should update `query` of new Query', function() {
      var query = new Query();
      var newQuery = query.limit('limit');
      expect(newQuery.query).to.have.property('max-results');
      expect(newQuery.query['max-results']).to.equal('limit');
    });
  });

  describe('#offset', function() {
    it('should return a new Query', function() {
      var query = new Query();
      var newQuery = query.offset();
      expect(newQuery).to.be.instanceOf(Query);
      expect(query).to.not.equal(newQuery);
    });

    it('should update `query` of new Query', function() {
      var query = new Query();
      var newQuery = query.offset('offset');
      expect(newQuery.query).to.have.property('start-index');
      expect(newQuery.query['start-index']).to.equal('offset');
    });
  });

  describe('#exec', function() {
    it('should query `worksheet` for rows', function(done) {
      var worksheet = {
        getRows: function(query, cb) {
          cb();
        }
      };
      sinon.spy(worksheet, 'getRows');

      var query = new Query(worksheet);
      query.exec(function() {
        expect(worksheet.getRows).to.have.been.called;
        done();
      });
    });

    it('should pass back an error if query fails', function(done) {
      var worksheet = {
        getRows: function(query, cb) {
          cb(new Error('failure'));
        }
      };

      var query = new Query(worksheet);
      query.exec(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        done();
      });
    });
  });
});
