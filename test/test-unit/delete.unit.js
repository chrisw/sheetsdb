'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Delete = require('../../lib/delete');
var Query = require('../../lib/query');

describe('Delete', function() {
  describe('@constructor', function() {
    it('should create an instance of Delete', function() {
      expect(new Delete()).to.be.instanceOf(Delete);
    });

    it('should create a copy of query if provided', function() {
      var del = new Delete(null, null, {query: {where: 'where'}});
      expect(del.query).to.be.instanceOf(Query);
    });
  });

  describe('#where', function() {
    it('should return a new Delete', function() {
      var del = new Delete();
      var newDelete = del.where({where: 'here'});
      expect(newDelete).to.be.instanceOf(Delete);
      expect(del).to.not.equal(newDelete);
    });

    it('should del `query` of Query', function() {
      var del = new Delete();
      var newDelete = del.where({where: 'here'});
      expect(newDelete.query.query.sq).to.equal('where = "here"');
    });
  });

  describe('#orderBy', function() {
    it('should return a new Delete', function() {
      var del = new Delete();
      var newDelete = del.orderBy();
      expect(newDelete).to.be.instanceOf(Delete);
      expect(del).to.not.equal(newDelete);
    });

    it('should del `query` of Query', function() {
      var del = new Delete();
      var newDelete = del.orderBy('orderBy');
      expect(newDelete.query.query.orderby).to.equal('orderBy');
    });
  });

  describe('#reverse', function() {
    it('should return a new Delete', function() {
      var del = new Delete();
      var newDelete = del.reverse();
      expect(newDelete).to.be.instanceOf(Delete);
      expect(del).to.not.equal(newDelete);
    });

    it('should del `query` of Query', function() {
      var del = new Delete();
      var newDelete = del.reverse();
      expect(newDelete.query.query.reverse).to.be.true;
    });
  });

  describe('#limit', function() {
    it('should return a new Delete', function() {
      var del = new Delete();
      var newDelete = del.limit();
      expect(newDelete).to.be.instanceOf(Delete);
      expect(del).to.not.equal(newDelete);
    });

    it('should del `query` of Query', function() {
      var del = new Delete();
      var newDelete = del.limit('limit');
      expect(newDelete.query.query['max-results']).to.equal('limit');
    });
  });

  describe('#offset', function() {
    it('should return a new Delete', function() {
      var del = new Delete();
      var newDelete = del.offset();
      expect(newDelete).to.be.instanceOf(Delete);
      expect(del).to.not.equal(newDelete);
    });

    it('should del `query` of Query', function() {
      var del = new Delete();
      var newDelete = del.offset('offset');
      expect(newDelete.query.query['start-index']).to.equal('offset');
    });
  });

  describe('#exec', function() {
    var rows;

    function del(cb) {
      cb();
    }

    var worksheet = {
      getRows: function(query, cb) {
        cb(null, rows);
      }
    };
    sinon.spy(worksheet, 'getRows');

    before(function() {
      del = sinon.spy(del);
    });

    beforeEach(function() {
      rows = [
        {del: del, attr: {}},
      ];

      worksheet.getRows.reset();
      del.reset();
    });

    it('should query `worksheet` for rows', function(done) {
      var del = new Delete(worksheet);
      del.exec(function() {
        expect(worksheet.getRows).to.have.been.called;
        done();
      });
    });

    it('should delete each row received by the query', function(done) {
      var del = new Delete(worksheet, {name: 'name'});
      del.exec(function() {
        done();
      });
    });

    it('should pass back an error if query fails', function(done) {
      var getRows = worksheet.getRows;
      worksheet.getRows = function(query, cb) {
        cb(new Error('failure'));
      };

      var del = new Delete(worksheet);
      del.exec(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        worksheet.getRows = getRows;
        done();
      });
    });
  });
});
