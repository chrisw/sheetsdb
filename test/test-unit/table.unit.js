'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Table = require('../../lib/table');
var Query = require('../../lib/query');
var Update = require('../../lib/update');
var Delete = require('../../lib/delete');

describe('Table', function() {
  describe('@constructor', function() {
    it('should create an instance of Table', function() {
      expect(new Table()).to.be.instanceOf(Table);
    });
  });

  describe('#insert', function() {
    it('should add rows through `worksheet`', function(done) {
      var worksheet = {
        addRow: function(rowObject, cb) {
          cb();
        }
      };
      sinon.spy(worksheet, 'addRow');

      var table = new Table(worksheet);
      table.insert(null, function() {
        expect(worksheet.addRow).to.have.been.called;
        done();
      });
    });

    it('should pass back error if add row fails', function(done) {
      var worksheet = {
        addRow: function(rowObject, cb) {
          cb(new Error('failure'));
        }
      };

      var table = new Table(worksheet);
      table.insert(null, function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        done();
      })
    });
  });

  describe('#select', function() {
    it('should return a Query object', function() {
      var table = new Table();
      var query = table.select();
      expect(query).to.be.instanceOf(Query);
    });
  });

  describe('#update', function() {
    it('should return an Update object', function() {
      var table = new Table();
      var update = table.update();
      expect(update).to.be.instanceOf(Update);
    });
  });

  describe('#delete', function() {
    it('should return a Delete object', function() {
      var table = new Table();
      var del = table.delete();
      expect(del).to.be.instanceOf(Delete);
    });
  });

  describe('#drop', function() {
    it('should delete worksheet', function(done) {
      var worksheet = {
        del: function(cb) {
          cb();
        }
      };
      sinon.spy(worksheet, 'del');

      var table = new Table(worksheet);
      table.drop(function() {
        expect(worksheet.del).to.have.been.called;
        done();
      });
    });

    it('should pass back an error if del fails', function(done) {
      var worksheet = {
        del: function(cb) {
          cb(new Error('failure'));
        }
      };

      var table = new Table(worksheet);
      table.drop(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        done();
      });
    });
  });
});
