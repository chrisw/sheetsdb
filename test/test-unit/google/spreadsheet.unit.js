'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var SpreadSheet = require('../../../lib/google/spreadsheet');
var WorkSheet = require('../../../lib/google/worksheet');

var Util = require('../../../lib/google/util');

describe('SpreadSheet', function() {
  describe('@constructor', function() {
    it('should create an instance of SpreadSheet', function() {
      expect(new SpreadSheet()).to.be.instanceOf(SpreadSheet);
    });
  });

  describe('#connect', function() {
    var creds = {
      client_email: 'email',
      private_key: 'key'
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();

      var pw = sandbox.stub(SpreadSheet.prototype, '_populateWorkSheets');
      pw.callsArg(0);
    });

    after(function() {
      sandbox.restore();
    });

    it('should create a JWT client', function(done) {
      var auth = sandbox.stub(SpreadSheet.prototype, '_authenticate');
      auth.callsArg(0);

      var spreadsheet = new SpreadSheet();

      spreadsheet.connect(creds, function() {
        expect(spreadsheet.client).to.exist;
        auth.restore();
        done();
      });
    });

    it('should authenticate the client', function(done) {
      var auth = sandbox.stub(SpreadSheet.prototype, '_authenticate');
      auth.callsArg(0);

      var spreadsheet = new SpreadSheet();

      spreadsheet.connect(creds, function() {
        expect(auth).to.have.been.called;
        auth.restore();
        done();
      });
    });

    it('should pass back an error if client cannot be authenticated', function(done) {
      var auth = sandbox.stub(SpreadSheet.prototype, '_authenticate');
      auth.callsArgWith(0, new Error('error'));

      var spreadsheet = new SpreadSheet();

      spreadsheet.connect(creds, function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        auth.restore();
        done();
      });
    });
  });

  describe('#_authenticate', function() {
    it('should authorize client to create token', function(done) {
      var spreadsheet = new SpreadSheet();
      spreadsheet.client = {
        authorize: function(cb) {
          cb(null, 'token');
        }
      };

      spreadsheet._authenticate(function(err) {
        expect(spreadsheet.token).to.equal('token');
        done();
      });
    });

    it('should pas back an error if authorization fails', function(done) {
      var spreadsheet = new SpreadSheet();
      spreadsheet.client = {
        authorize: function(cb) {
          cb(new Error('error'));
        }
      };

      spreadsheet._authenticate(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        done();
      });
    });
  });

  describe('#_populateWorkSheets', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(SpreadSheet.prototype, '_dataToWorkSheets');
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
    });

    it('should make a GET feed request', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, null, 'data');

      var spreadsheet = new SpreadSheet();
      spreadsheet.token = 'token';

      spreadsheet._populateWorkSheets(function() {
        expect(fr).to.have.been.calledWith('token');
        expect(fr.args[0][1].method).to.equal('GET');
        fr.restore();
        done();
      });
    });

    it('should make a feed request with the appropriate url', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, null, 'data');

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';

      spreadsheet._populateWorkSheets(function() {
        expect(fr).to.have.been.calledWith('token');
        expect(fr.args[0][1].url).to.eql(['worksheets', 'key']);
        fr.restore();
        done();
      });
    });

    it('should pass back an error if feed request fails', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, new Error('error'));

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';

      spreadsheet._populateWorkSheets(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        fr.restore();
        done();
      });
    });

    it('should pass back an error if bad data received', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, null, null);

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';

      spreadsheet._populateWorkSheets(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('No worksheets response');
        fr.restore();
        done();
      });
    });
  });

  describe('#_dataToWorkSheets', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks');
    });

    after(function() {
      sandbox.restore();
    });

    it('should convert data to WorkSheets', function() {
      var data = {
        entry: {id: ['id'], title: [{'_': 'title'}]}
      };

      var spreadsheet = new SpreadSheet();
      var worksheets = spreadsheet._dataToWorkSheets(data);
      expect(worksheets).to.not.be.empty;
      expect(worksheets[0]).to.be.instanceOf(WorkSheet);
    });

    it('should return an empty array if no worksheets are found', function() {
      var data = {
        entry: null
      };

      var spreadsheet = new SpreadSheet();
      var worksheets = spreadsheet._dataToWorkSheets(data);
      expect(worksheets).to.be.empty;
    });
  });

  describe('#createWorkSheet', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
    });

    afterEach(function() {
      sandbox.reset();
    });

    after(function() {
      sandbox.restore();
    });

    it('should make a POST feed request', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2)

      var pw = sandbox.stub(SpreadSheet.prototype, '_populateWorkSheets');
      pw.callsArg(0);

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';

      spreadsheet.createWorkSheet('title', 0, function(err, worksheet) {
        expect(fr).to.be.calledWith('token');
        expect(fr.args[0][1].method).to.equal('POST');
        fr.restore();
        pw.restore();
        done();
      });
    });

    it('should make a feed request with the appropriate url', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2)

      var pw = sandbox.stub(SpreadSheet.prototype, '_populateWorkSheets');
      pw.callsArg(0);

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';

      spreadsheet.createWorkSheet('title', 0, function(err, worksheet) {
        expect(fr).to.be.calledWith('token');
        expect(fr.args[0][1].url).to.eql(['worksheets', 'key']);
        fr.restore();
        pw.restore();
        done();
      });
    });

    it('should pass back the created worksheet', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2)

      var pw = sandbox.stub(SpreadSheet.prototype, '_populateWorkSheets');
      pw.callsArg(0);

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';
      spreadsheet.worksheets = ['worksheet'];

      spreadsheet.createWorkSheet('title', 0, function(err, worksheet) {
        expect(worksheet).to.equal('worksheet');
        fr.restore();
        pw.restore();
        done();
      });
    });

    it('should pass back an error if feed request fails', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, new Error('error'))

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';
      spreadsheet.worksheets = ['worksheet'];

      spreadsheet.createWorkSheet('title', 0, function(err, worksheet) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        fr.restore();
        done();
      });
    });

    it('should pass back an error if worksheets cannot be populated', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2)

      var pw = sandbox.stub(SpreadSheet.prototype, '_populateWorkSheets');
      pw.callsArgWith(0, new Error('error'));

      var spreadsheet = new SpreadSheet('key');
      spreadsheet.token = 'token';
      spreadsheet.worksheets = ['worksheet'];

      spreadsheet.createWorkSheet('title', 0, function(err, worksheet) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        fr.restore();
        pw.restore();
        done();
      });
    });
  });
});
