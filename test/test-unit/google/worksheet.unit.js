'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var WorkSheet = require('../../../lib/google/worksheet');
var Row = require('../../../lib/google/row');

var Util = require('../../../lib/google/util');

describe('WorkSheet', function() {
  describe('@constructor', function() {
    var data = {
      id: ['some/url'],
      title: [{'_': 'title'}]
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks');
    });

    after(function() {
      sandbox.restore();
    });

    it('should create an instance of WorkSheet', function() {
      expect(new WorkSheet(null, data)).to.be.instanceOf(WorkSheet);
    });
  });

  describe('#_extractLinks', function() {
    var data = {
      id: ['some/url'],
      title: [{'_': 'title'}],
      link: {'$': {rel: 'edit', href: 'spreadsheets.google.com/edit'}}
    };

    it('should extract links from xml data', function() {
      var worksheet = new WorkSheet(null, data);
      expect(worksheet.links).to.not.be.empty;
      expect(worksheet.links.edit).to.equal('spreadsheets.google.com/edit');
    });
  });

  describe('#getRows', function() {
    var spreadsheet = {
      token: {expiry_date: 0},
      key: 'key',
      _authenticate: function(cb) {
        cb();
      }
    };

    var data = {
      id: ['some/id'],
      title: [{'_': 'title'}]
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks');

      var dtr = sandbox.stub(WorkSheet.prototype, '_dataToRows');
      dtr.callsArg(1);
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
    });

    it('should make a GET feed request', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);

      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.getRows('query', function() {
        expect(fr.args[0][1].method).to.equal('GET');
        fr.restore();
        done();
      });
    });

    it('should make a feed request with the proper url', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);

      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.getRows('query', function() {
        expect(fr.args[0][1].url).to.eql(['list', 'key', 'id']);
        fr.restore();
        done();
      });
    });

    it('should convert the data recieved to rows', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);

      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.getRows('query', function() {
        expect(worksheet._dataToRows).to.have.been.called;
        fr.restore();
        done();
      });
    });

    it('should pass back an error if feed request fails', function(done) {
      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArgWith(2, new Error('error'));

      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.getRows('query', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        fr.restore();
        done();
      });
    });
  });

  describe('#_dataToRows', function() {
    var data = {
      id: ['some/id'],
      title: [{'_': 'title'}]
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks');

      sandbox.stub(Row.prototype, '_extractLinks');
      sandbox.stub(Row.prototype, '_extractAttributes');
    });

    after(function() {
      sandbox.restore();
    });

    it('should convert data to rows', function(done) {
      var result = {
        entry: {rowInfo: 'info'}
      };

      var worksheet = new WorkSheet(null, data);

      worksheet._dataToRows(result, function(err, rows) {
        expect(rows).to.not.be.empty;
        expect(rows[0]).to.be.instanceOf(Row);
        done();
      });
    });

    it('should return an emtpy array if no rows are found', function(done) {
      var result = {
        entry: null
      };

      var worksheet = new WorkSheet(null, data);

      worksheet._dataToRows(result, function(err, rows) {
        expect(rows).to.be.empty;
        done();
      });
    });
  });

  describe('#addRow', function() {
    var spreadsheet = {
      token: {expiry_date: 0},
      key: 'key',
      _authenticate: function(cb) {
        cb();
      }
    };

    var data = {
      id: ['some/id'],
      title: [{'_': 'title'}]
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks');

      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);
    });

    after(function() {
      sandbox.restore();
    });

    it('should make a POST feed request', function(done) {
      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.addRow({attr: 'value'}, function() {
        expect(Util.feedRequest.args[0][1].method).to.equal('POST');
        done();
      });
    });

    it('should make a feed request with the proper url', function(done) {
      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.addRow({attr: 'value'}, function() {
        expect(Util.feedRequest.args[0][1].url).to.eql(['list', 'key', 'id']);
        done();
      });
    });

    it('should make a feed request with the proper body', function(done) {
      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.addRow({attr: 'value'}, function() {
        expect(Util.feedRequest.args[0][1].body)
          .to.contain('<gsx:attr>value</gsx:attr>');
        done();
      });
    });
  });

  describe('#del', function() {
    var spreadsheet = {
      token: {expiry_time: 0},
      _authenticate: function(cb) {
        cb();
      }
    };

    var data = {
      id: ['some/id'],
      title: [{'_': 'title'}]
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(WorkSheet.prototype, '_extractLinks', function() {
        var self = this;
        self.links = {
          edit: 'edit'
        };
      });

      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);
    });

    after(function() {
      sandbox.restore();
    });

    it('should make DELETE feed request', function(done) {
      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.del(function() {
        expect(Util.feedRequest.args[0][1].method).to.equal('DELETE');
        done();
      });
    });

    it('should make a feed request with the worksheet url', function(done) {
      var worksheet = new WorkSheet(spreadsheet, data);

      worksheet.del(function() {
        expect(Util.feedRequest.args[0][1].url).to.equal('edit');
        done();
      });
    });
  });
});
