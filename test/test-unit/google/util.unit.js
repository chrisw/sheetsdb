'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Util;
var request;
var xml2js;

before(function() {
  request = sinon.stub();
  xml2js = sinon.stub();
  Util = proxyquire('../../../lib/google/util', {
    'request': request,
    'xml2js': {
      Parser: function() {
        this.parseString = xml2js;
      }
    }
  });
});

describe('Util', function() {
  describe('renewAuth', function() {
    var clock;

    before(function() {
      clock = sinon.useFakeTimers();
    });

    after(function() {
      clock.restore();
    });

    it('should decorate a function passed in', function() {
      var func = Util.renewAuth(function() {});
      expect(func).to.be.instanceOf(Function);
    });

    it('should simply call the function if expiry date has not passed', function(done) {
      var spreadsheet = {
        token: {expiry_date: 1},
        _authenticate: sinon.spy()
      };

      var self = {
        spreadsheet: spreadsheet,
        func: function(cb) {
          cb();
        }
      };

      self.func = Util.renewAuth(self.func);
      self.func(function() {
        expect(spreadsheet._authenticate).to.not.have.been.called;
        done();
      });
    });

    it('should authenticate function if expiry date has passed', function(done) {
      var spreadsheet = {
        token: {expiry_date: -1},
        _authenticate: sinon.stub()
      };
      spreadsheet._authenticate.callsArg(0);

      var self = {
        spreadsheet: spreadsheet,
        func: function(cb) {
          cb();
        }
      };

      self.func = Util.renewAuth(self.func);
      self.func(function() {
        expect(spreadsheet._authenticate).to.have.been.called;
        done();
      });
    });

    it('should pass back an error if authentication fails', function(done) {
      var spreadsheet = {
        token: {expiry_date: -1},
        _authenticate: sinon.stub()
      };
      spreadsheet._authenticate.callsArgWith(0, new Error('error'));

      var self = {
        spreadsheet: spreadsheet,
        func: function(cb) {
          cb();
        }
      };

      self.func = Util.renewAuth(self.func);
      self.func(function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        done();
      });
    });

    it('should throw an error if authentication fails', function() {
      var spreadsheet = {
        token: {expiry_date: -1},
        _authenticate: sinon.stub()
      };
      spreadsheet._authenticate.callsArgWith(0, new Error('error'));

      var self = {
        spreadsheet: spreadsheet,
        func: function() {}
      };

      self.func = Util.renewAuth(self.func);
      expect(self.func.bind(self)).to.throw(Error, /error/);
    });
  });

  describe('feedRequest', function() {
    afterEach(function() {
      request.reset();
      xml2js.reset();
    });

    it('should make a request with the method specified', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET'
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].method).to.equal('GET');
        done();
      });
    });

    it('should make a request with the bearer header', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET'
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].headers).to.have.property('Authorization');
        expect(request.args[0][0].headers.Authorization).to.equal('Bearer token');
        done();
      });
    });

    it('should set the content header if POST or PUT request', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'POST'
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].headers).to.have.property('content-type');
        expect(request.args[0][0].headers['content-type']).to.equal('application/atom+xml');
        done();
      });
    });

    it('should make a request with the body specified', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'POST',
        body: 'body'
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].body).to.equal('body');
        done();
      });
    });

    it('should use the url given if specified', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'POST',
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].url).to.equal('url');
        done();
      });
    });

    it('should build the url if provided by an array', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: ['url'],
        method: 'POST',
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].url)
          .to.equal('https://spreadsheets.google.com/feeds/url/private/full');
        done();
      });
    });

    it('should append a query string the url if provided', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET',
        query: {a: 1}
      };

      Util.feedRequest(token, options, function() {
        expect(request).to.have.been.called;
        expect(request.args[0][0].url).to.equal('url?a=1');
        done();
      });
    });

    it('should pass back the parsed feed', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET',
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(request).to.have.been.called;
        expect(data).to.have.property('a');
        expect(data.a).to.eql(['1']);
        done();
      });
    });

    it('should pass back an error if request fails', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, new Error('error'));
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET',
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        done();
      });
    });

    it('should pass back an error if request status not ok', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 400}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET',
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('Request failed with status 400');
        done();
      });
    });

    it('should pass back an error if url is not provided', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        method: 'GET',
      };

      Util.feedRequest(token, options, function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('Request Feed must be given url params');
        done();
      });
    });

    it('should pass back an error if method is not provided', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('Request Feed must be given http method');
        done();
      });
    });

    it('should pass back an error if xml parsing fails', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, body);
      xml2js.callsArgWith(1, new Error('error'));

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET'
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        done();
      });
    });

    it('should not parse body if body is not given', function(done) {
      var body = '<feed><a>1</a></feed>';
      request.callsArgWith(1, null, {statusCode: 200}, null);
      xml2js.callsArgWith(1, null, {feed: {a: ['1']}});

      var token = {access_token: 'token'};

      var options = {
        url: 'url',
        method: 'GET',
      };

      Util.feedRequest(token, options, function(err, data) {
        expect(data).to.not.be.ok;
        done();
      });
    });
  });

  describe('xmlSafe', function() {
    it('should remove spaces', function() {
      expect(Util.xmlSafe('this is a test')).to.equal('thisisatest');
    });

    it('should convert to lower case', function() {
      expect(Util.xmlSafe('someID')).to.equal('someid');
    });

    it('should return an empty string for a falsy value', function() {
      expect(Util.xmlSafe(null)).to.equal('');
    });
  });

  describe('forceArray', function() {
    it('should coerce a value into an array of that value', function() {
      expect(Util.forceArray(5)).to.eql([5]);
    });

    it('should not affect arrays', function() {
      expect(Util.forceArray([5])).to.eql([5]);
    });
  });

  describe('forceSingle', function() {
    it('should coerce an array into a single value', function() {
      expect(Util.forceSingle([5])).to.equal(5);
    });

    it('should not affect single values', function() {
      expect(Util.forceSingle(5)).to.equal(5);
    });
  });
});
