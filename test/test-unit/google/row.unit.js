'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Row = require('../../../lib/google/row');
var Util = require('../../../lib/google/util');

describe('Row', function() {
  describe('@constructor', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Row.prototype, '_extractLinks');
      sandbox.stub(Row.prototype, '_extractAttributes');
    });

    after(function() {
      sandbox.restore();
    });

    it('should create an instance of Row', function() {
      expect(new Row(null, null)).to.be.instanceOf(Row);
    });
  });

  describe('#_extractAttributes', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Row.prototype, '_extractLinks');
    });

    after(function() {
      sandbox.restore();
    });

    it('should extract attributes out of xml data', function() {
      var data = {
        'gsx:key': 'value'
      };

      var row = new Row(null, data);
      expect(row.attr).to.exist;
      expect(row.attr.key).to.equal('value');
    });

    it('should only extract attributes', function() {
      var data = {
        'key': 'value'
      };

      var row = new Row(null, data);
      expect(row.attr).to.exist;
      expect(row.attr).to.be.empty;
    });
  });

  describe('#_extractLinks',function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Row.prototype, '_extractAttributes');
    });

    after(function() {
      sandbox.restore();
    });

    it('should extract links out of xml data', function() {
      var data = {
        link: {'$': {rel: 'edit', href: 'edit'}}
      };

      var row = new Row(null, data);
      expect(row.links).to.exist;
      expect(row.links.edit).to.equal('edit');
    });
  });

  describe('#get', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();

      sandbox.stub(Row.prototype, '_extractLinks');
      sandbox.stub(Row.prototype, '_extractAttributes', function() {
        var self = this;
        self.attr = {
          userid: 'user'
        };
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should return the atribute value of the xml store', function() {
      var row = new Row();
      expect(row.get('userId')).to.equal('user');
    });
  });

  describe('#set', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();

      sandbox.stub(Row.prototype, '_extractLinks');
      sandbox.stub(Row.prototype, '_extractAttributes', function() {
        var self = this;
        self.attr = {
          userid: 'user'
        };
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should modify the attribute stored in xml', function() {
      var row = new Row();

      row.set('userId', 'newUser');
      expect(row.attr.userid).to.equal('newUser');
    });
  });

  describe('#save', function() {
    var spreadsheet = {
      token: {expiry_date: 0},
      _authenticate: function(cb) {
        cb();
      }
    };
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Row.prototype, '_extractLinks', function() {
        var self = this;
        self.links = {
          edit: 'edit'
        }
      });

      sandbox.stub(Row.prototype, '_extractAttributes', function() {
        var self = this;
        self.attr = {a: 1};
      });

      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2);
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
    });

    it('should make a PUT feed request', function(done) {
      var data = {};
      var row = new Row(spreadsheet, data);

      row.save(function() {
        expect(Util.feedRequest.args[0][1].method).equals('PUT');
        done();
      });
    });

    it('should make a feed request with the appropriate url', function(done) {
      var data = {};
      var row = new Row(spreadsheet, data);

      row.save(function() {
        expect(Util.feedRequest.args[0][1].url).equals('edit');
        done();
      });
    });

    it('should make a feed request with the proper body', function(done) {
      var data = {};
      var row = new Row(spreadsheet, data);

      row.save(function() {
        expect(Util.feedRequest.args[0][1].body).to.contain('<gsx:a>1</gsx:a>');
        done();
      });
    });
  });

  describe('#del', function() {
    var spreadsheet = {
      token: {expiry_date: 0},
      _authenticate: function(cb) {
        cb();
      }
    };

    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();

      sandbox.stub(Row.prototype, '_extractAttributes');
      sandbox.stub(Row.prototype, '_extractLinks', function() {
        var self = this;
        self.links = {
          edit: 'edit'
        };
      });

      var fr = sandbox.stub(Util, 'feedRequest');
      fr.callsArg(2)
    });

    after(function() {
      sandbox.restore();
    });

    it('should make DELETE feed request', function(done) {
      var row = new Row(spreadsheet, null);

      row.del(function() {
        expect(Util.feedRequest.args[0][1].method).to.equal('DELETE');
        done();
      });
    });

    it('should make a feed request with the row url', function(done) {
      var row = new Row(spreadsheet, null);

      row.del(function() {
        expect(Util.feedRequest.args[0][1].url).to.equal('edit');
        done();
      });
    });
  });
});
