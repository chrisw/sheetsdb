'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var Database = require('../../lib/database');
var Table = require('../../lib/table');
var SpreadSheet = require('../../lib/google/spreadsheet');

describe('Database', function() {
  describe('.connect', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables');
    });

    after(function() {
      sandbox.restore();
    });

    it('should connect to the a spreadsheet', function(done) {
      var connect = sinon.stub(SpreadSheet.prototype, 'connect', function(creds, cb) {
        cb();
      });

      Database.connect(null, null, function() {
        expect(connect).to.have.been.called;
        connect.restore();
        done();
      });
    });

    it('should pass back a new instance of Database', function(done) {
      var connect = sinon.stub(SpreadSheet.prototype, 'connect', function(creds, cb) {
        cb();
      });

      Database.connect(null, null, function(err, db) {
        expect(db).to.be.instanceOf(Database);
        connect.restore();
        done();
      });
    });

    it('should pass back an error if connect fails', function(done) {
      var connect = sinon.stub(SpreadSheet.prototype, 'connect', function(creds, cb) {
        cb(new Error('failure'));
      });

      Database.connect(null, null, function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        connect.restore();
        done();
      });
    });
  });

  describe('@constructor', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables');
    });

    after(function() {
      sandbox.restore();
    });

    it('should create a new instane of Database', function() {
      expect(new Database()).to.be.instanceOf(Database);
    });
  });

  describe('#_setTables', function() {
    var spreadsheet = {
      worksheets: [{title: 'title'}]
    };

    it('should add worksheets to `tables`', function() {
      var db = new Database(spreadsheet);
      expect(db.tables).to.have.property('title');
    });
  });

  describe('#create', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {title: 'exists'};
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should create a new worksheet', function(done) {
      var spreadsheet = {
        createWorkSheet: function(tableName, numCols, cb) {
          cb(null, 'worksheet');
        }
      };
      sinon.spy(spreadsheet, 'createWorkSheet');

      var db = new Database(spreadsheet);
      db.create('notExists', function() {
        expect(spreadsheet.createWorkSheet).to.have.been.called;
        done();
      });
    });

    it('should pass back a new instance of Table', function(done) {
      var spreadsheet = {
        createWorkSheet: function(tableName, numCols, cb) {
          cb(null, 'worksheet');
        }
      };

      var db = new Database(spreadsheet);
      db.create('notExist', function(err, table) {
        expect(table).to.be.instanceOf(Table);
        done();
      });
    });

    it('should pass back an error if the worksheet already exists', function(done) {
      var spreadsheet = {
        createWorkSheet: function(tableName, numCols, cb) {
          cb(null, 'worksheet');
        }
      };

      var db = new Database(spreadsheet);
      db.create('exists', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('Table: exists already exists');
        done();
      });
    });

    it('should pass back an error if create fails', function(done) {
      var spreadsheet = {
        createWorkSheet: function(tableName, numCols, cb) {
          cb(new Error('failure'));
        }
      };

      var db = new Database(spreadsheet);
      db.create('notExists', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        done();
      });
    });
  });

  describe('#insert', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {};
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should call a table insert', function(done) {
      var db = new Database();
      db.tables.exists.insert = function(rowObject, cb) {
        cb();
      };

      db.insert('exists', null, function() {
        done();
      });
    });

    it('should throw an error if table does not exist', function() {
      var db = new Database();
      expect(db.insert.bind(db, 'notExists')).to.throw(Error, /Table: notExists not found/);
    });
  });

  describe('#select', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          select: function() {
            return 'select';
          }
        };
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should return a table select', function() {
      var db = new Database();
      var query = db.select('exists');
      expect(query).to.equal('select');
    });

    it('should throw an error if table does not exist', function() {
      var db = new Database();
      expect(db.select.bind(db, 'notExists')).to.throw(Error, /Table: notExists not found/);
    });
  });

  describe('#update', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          update: function() {
            return 'update';
          }
        };
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should return a table update', function() {
      var db = new Database();
      var update = db.update('exists');
      expect(update).to.equal('update');
    });

    it('should throw an error if table does not exist', function() {
      var db = new Database();
      expect(db.update.bind(db, 'notExists')).to.throw(Error, /Table: notExists not found/);
    });
  });

  describe('#delete', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          delete: function() {
            return 'delete';
          }
        };
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should return a table delete', function() {
      var db = new Database();
      var del = db.delete('exists');
      expect(del).to.equal('delete');
    });

    it('should throw an error if table does not exist', function() {
      var db = new Database();
      expect(db.delete.bind(db, 'notExists')).to.throw(Error, /Table: notExists not found/);
    });
  });

  describe('#drop', function() {
    it('should drop the specified table', function(done) {
      var _setTables = sinon.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          drop: function(cb) {
            cb();
          }
        };
      });

      var db = new Database();
      db.drop('exists', function() {
        expect(db.tables).to.not.have.property('exists');
        _setTables.restore();
        done();
      });
    });

    it('should pass back an error of table does not exist', function(done) {
      var _setTables = sinon.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          drop: function(cb) {
            cb();
          }
        };
      });

      var db = new Database();
      db.drop('notExists', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('Table: notExists not found');
        _setTables.restore();
        done();
      });
    });

    it('should pass back an error if drop fails', function(done) {
      var _setTables = sinon.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = {
          drop: function(cb) {
            cb(new Error('failure'));
          }
        };
      });

      var db = new Database();
      db.drop('exists', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('failure');
        _setTables.restore();
        done();
      });
    });
  });

  describe('#table', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(Database.prototype, '_setTables', function() {
        var self = this;
        self.tables.exists = 'table';
      });
    });

    after(function() {
      sandbox.restore();
    });

    it('should return the table specified', function() {
      var db = new Database();
      var table = db.table('exists');
      expect(table).to.equal('table');
    });

    it('should throw an error if table does not exist', function(){
      var db = new Database();
      expect(db.table.bind(db, 'notExists')).to.throw(Error, /Table: notExists not found/);
    });
  });
});
